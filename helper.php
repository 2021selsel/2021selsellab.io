<?php

function randomProducts( $products = [] )
{
    shuffle( $products );
    return array_slice( $products, 0, 20 );
}


function random_prefix()
{
    $prefix = ["Wholesale","SaleStore","Flash Sale"];

    return $prefix[array_rand($prefix)];
}


function arr_get( $array, string $key, $fallback = null)
{
	if ( ! $array ) {
		return $fallback;
	}
	
	if ( isset ( $array[$key] ) ) {
		return $array[$key];
	}
	
    $keys = explode('.', $key);
    
    foreach ($keys as $key) {
        if ( ! is_array($array) || ! array_key_exists($key, $array)) {
            return $fallback;
        }
        
        $array = &$array[$key];
    }

    return $array;
}
