<?php

define( "BLOGNAME",         "2021selsel.gitlab.io" );
define( "SUBDOMAIN_OFFER",  "abc.example.com" );
define( "WEBMASTER",        '' );

define( "BASE", __DIR__ );

require_once BASE."/helper.php";
require_once BASE."/minifier.php";

$products = json_decode( gzinflate( file_get_contents( BASE . "/products" ) ), true );


    $oldmask = umask(0);
	mkdir( BASE . "/.public/items/", 0777, true );
	umask( $oldmask );

    $_totalArray 	= 	count( $products ); 
	$_post_per_page =	200;
	$_totalPages 	= 	ceil( $_totalArray / $_post_per_page );

    $_sitemap 		= 	"";


    //create pagination
    for( $pagination=1; $pagination <= $_totalPages; $pagination++ ) {
    
        $arr_offset_items = ( $pagination - 1 ) * $_post_per_page;
        $arr_offset_items = $arr_offset_items ?: 0;

        $arr_per_pages = array_slice( $products, $arr_offset_items, $_post_per_page );

        if( $pagination > 2 ) {

			$prevnum = ( $pagination - 1 );
			$nextnum = ( $pagination + 1 );
			
			$prevUrl = "/page-{$prevnum}.html";
			$nextUrl = "/page-{$nextnum}.html";
			
		} else {
			
			$prevUrl = "/index.html";
			$nextUrl = "/page-2.html";
			
		}

        //current filename
		if ( $pagination === 1 ) {
			$currentFilename = "index.html";
		} else {
			$currentFilename = "page-{$pagination}.html";
		}

        $_sitemap 	.= 	"https://".BLOGNAME."/{$currentFilename}\n";

        $file_location	=	BASE . "/.public/{$currentFilename}";

        //html output.
		ob_start();
		include BASE . "/page-template.php";
		$html_template = Minifier::html( ob_get_contents() );
		ob_end_clean();

        @file_put_contents( $file_location, $html_template );

    }


    //create single products
    foreach( $products as $items ) {

        $productId 			= 	arr_get( $items, 'productId' );
		$productTitle 		= 	arr_get( $items, 'productTitle' );
		$salePrice 			= 	arr_get( $items, 'salePrice' );
		$imageUrl 			= 	arr_get( $items, 'imageUrl' ) . "?quality=75";
        $canonical			=	"https://".BLOGNAME."/items/{$productId}.html";
        $productTitle 		= 	htmlentities( $productTitle );
        $file_location		=	BASE . "/.public/items/{$productId}.html";
		$single_title		=	random_prefix()." {$salePrice} {$productTitle} {$productId} | ".BLOGNAME;
		$single_desc		=	"{$single_title} : {$single_title}";

        $_sitemap           .=   $canonical;

        $splitProduct		=	randomProducts( $products );

		ob_start();
			include BASE . "/single-template.php";
			$html_template = Minifier::html( ob_get_contents() ); 
		ob_end_clean();

        @file_put_contents( $file_location, $html_template );

    }


    $robots_txt = "User-agent: *\nAllow: /\n\nSitemap: https://".BLOGNAME."/sitemap.txt";

    @file_put_contents( BASE . "/.public/robots.txt", $robots_txt );

    @file_put_contents( BASE . "/.public/sitemap.txt", $_sitemap );








